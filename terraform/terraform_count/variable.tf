variable "ami_id" {
  description = "give the ami_id"
  type        = list(string)
  default     = ["ami-03f8756d29f0b5f21", "ami-03f8756d29f0b5f21"]

}

variable "instance_type" {
  description = "give the instance_type"
  type        = list(string)
  default     = ["t2.micro", "t3.micro"]

}