resource "aws_instance" "Instance" {
  count         = 2
  ami           = var.ami_id[count.index]
  instance_type = var.instance_type[count.index]

  tags = {
    Name = "Instance-1"
  }
}

