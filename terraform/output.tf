output "vpc_id" {
  value = aws_vpc.main.id

}

output "pub_subnet_id" {
  value = aws_subnet.main.id

}

output "pvt_subnet_id" {
  value = aws_subnet.pvt.id

}

output "igw_id" {
  value = aws_internet_gateway.igw.id

}

output "allocation_id" {
  value = aws_eip.elastic_ip.allocation_id

}

output "pub_rtb_id" {
  value = aws_route_table.Public-RTB.id

}

output "nat_id" {
  value = aws_nat_gateway.nat_gateway.id

}

output "pvt_rtb_id" {
  value = aws_route_table.Private-RTB.id

}

output "sg_id" {
  value = aws_security_group.my-sg-01.id

}