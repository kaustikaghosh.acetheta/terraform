variable "cidr_block" {
  description = "give the cidr value"
  type        = string
  default     = "10.0.0.0/16"

}

variable "cidr_block_pub" {
  description = "give the cidr value of Public-Subnet"
  type        = string
  default     = "10.0.1.0/24"

}

variable "vpc_id" {
  description = "give the vpc_id"
  type        = string
  default     = "vpc-072d1696b98c8e434"

}

variable "cidr_block_pvt" {
  description = "give the cidr value of Private-Subnet"
  type        = string
  default     = "10.0.2.0/24"

}

variable "allocation_id" {
  description = "give the elastic_ip"
  type        = string
  default     = "eipalloc-093098254a987b37f"

}

variable "pub_subnet_id" {
  description = "give the pub_subnet_id"
  type        = string
  default     = "subnet-0a6ca8b1b38218d1f"

}

variable "pvt_subnet_id" {
  description = "give the pvt_subnet_id"
  type        = string
  default     = "subnet-03a8b45344b5bf23f"

}

variable "igw_id" {
  description = "give the igw_id"
  type        = string
  default     = "igw-0e9e00cf83b4d13a2"

}

variable "nat_id" {
  description = "give the nat_id"
  type        = string
  default     = "nat-077342b927717aee3"

}

variable "pub_rtb_id" {
  description = "give the pub_rtb_id"
  type        = string
  default     = "rtb-00ae91e1527b32df3"

}

variable "pvt_rtb_id" {
  description = "give the pvt_rtb_id"
  type        = string
  default     = "rtb-0152cc276fe5f5cd3"

}

variable "ami_id" {
  description = "give the ami_id"
  type        = string
  default     = "ami-03f8756d29f0b5f21"

}

variable "instance_type" {
  description = "give the instance_type"
  type        = string
  default     = "t2.micro"

}

variable "sg_id" {
  description = "give the sg_id"
  type        = string
  default     = "sg-066663432777c5932"

}

