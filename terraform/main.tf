resource "aws_vpc" "main" {
  cidr_block = var.cidr_block

  tags = {
    Name = "Kaustika_VPC"
  }
}
resource "aws_subnet" "main" {
  vpc_id     = var.vpc_id
  cidr_block = var.cidr_block_pub

  tags = {
    Name = "Public-Subnet"
  }
}

resource "aws_subnet" "pvt" {
  vpc_id     = var.vpc_id
  cidr_block = var.cidr_block_pvt

  tags = {
    Name = "Private-Subnet"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = var.vpc_id

  tags = {
    Name = "Internet-Gateway"
  }
}

resource "aws_eip" "elastic_ip" {
  vpc      = true
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = var.allocation_id
  subnet_id     = var.pub_subnet_id

  tags = {
    Name = "Nat-Gateway"
    }
  }

  resource "aws_route_table" "Public-RTB" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.igw_id
  }

  tags = {
    Name = "Public-RTB"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = var.pub_subnet_id
  route_table_id = var.pub_rtb_id
}

resource "aws_route_table" "Private-RTB" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = var.nat_id
  }

  tags = {
    Name = "Private-RTB"
  }
}

resource "aws_route_table_association" "b" {
  subnet_id      = var.pvt_subnet_id
  route_table_id = var.pvt_rtb_id
}

resource "aws_security_group" "my-sg-01" {
  name        = "my-sg-01"
  description = "Allow http inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description      = "Http to VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }

  ingress {
    description      = "Https to VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }

   ingress {
    description      = "ssh to VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

    egress {
    description      = "Http to VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }

    egress {
    description      = "Https to VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }

    egress {
    description      = "ssh to VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  tags = {
    Name = "my-sg-01"
  }
}
 
resource "aws_instance" "Pub-Instance" {
    ami           = var.ami_id
    instance_type = var.instance_type
    subnet_id = var.pub_subnet_id
    vpc_security_group_ids= [var.sg_id]
    associate_public_ip_address=true
    
    tags = {
       Name = "Public-Instance"
    }
 }  

 resource "aws_s3_bucket" "terraform-bucket" {
  bucket = "sholanki-bucket"

  tags = {
    Name        = "sholanki-bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.terraform-bucket.id
  acl    = "private"
} 


