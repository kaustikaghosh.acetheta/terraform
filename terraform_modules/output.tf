output "vpc" {
  value = module.vpc.vpc_id

}
output "subnet" {
  value = module.subnet.subnet_id

}