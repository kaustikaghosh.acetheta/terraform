module "vpc" {
  source           = "./modules/vpc"
  vpc_cidr         = var.kaus_vpc_cidr
  instance_tenancy = var.kaus_instance_tenancy
  vpc_tags         = var.kaus_tag
}

module "subnet" {
  source      = "./modules/subnet"
  my_vpc_id   = module.vpc.vpc_id
  subnet_cidr = var.kaus_subnet_cidr
  subnet_tags = var.kaus_sub_tag

}