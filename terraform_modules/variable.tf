variable "kaus_vpc_cidr" {
  description = "give the cidr value of vpc"
  type        = string
  default     = "10.0.0.0/20"

}
variable "kaus_instance_tenancy" {
  description = "give the instance_tenancy"
  type        = string
  default     = "default"

}

variable "kaus_tag" {
  description = "give the tag of vpc"
  type        = string
  default     = "bye vpc"

}

variable "kaus_subnet_cidr" {
  description = "give the cidr value of subnet"
  type        = string
  default     = "10.0.2.0/16"

}

variable "kaus_sub_tag" {
  description = "give the tag of subnet"
  type        = string
  default     = "bye subnet"

}


