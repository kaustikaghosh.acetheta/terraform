variable "vpc_cidr" {
  description = "give the cidr value of vpc"
  type        = string
  default     = "10.0.0.0/16"

}
variable "instance_tenancy" {
  description = "give the instance_tenancy"
  type        = string
  default     = "default"

}

variable "vpc_tags" {
  description = "give the tag of vpc"
  type        = string
  default     = "hello vpc"

}
