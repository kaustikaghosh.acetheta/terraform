resource "aws_subnet" "subnet" {
  vpc_id     = var.my_vpc_id
  cidr_block = var.subnet_cidr

  tags = {
    Name = var.subnet_tags
  }
}