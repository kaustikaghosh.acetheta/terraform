variable "subnet_cidr" {
  description = "give the cidr value of subnet"
  type        = string
  default     = "10.0.1.0/16"

}

variable "subnet_tags" {
  description = "give the tag of subnet"
  type        = string
  default     = "hello subnet"

}

variable "my_vpc_id" {
  description = "give the vpc_id"
  type        = string
  default     = "hellomyvpc"

}
