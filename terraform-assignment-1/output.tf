output "vpc_id" {
  value = aws_vpc.my-vpc.id
}
output "Pub-Subnet-1" {
  value = aws_subnet.public[0].id
}
output "Pub-Subnet-2" {
  value = aws_subnet.public[1].id
}
output "Priv-Subnet-1" {
  value = aws_subnet.private[0].id
}
output "Priv-Subnet-2" {
  value = aws_subnet.private[1].id
}
output "IGW-ID" {
  value = aws_internet_gateway.igw.id
}
output "allocation_id" {
  value = aws_eip.elastic_ip.allocation_id
}
output "NAT_ID" {
  value = aws_nat_gateway.ninja_nat.id
}
output "Pub_RTB_ID" {
  value = aws_route_table.Public-RTB.id
}
output "Priv_RTB_ID" {
  value = aws_route_table.Private-RTB.id
}
output "vpc_security_group_id" {
  value = aws_security_group.my-sg-01.id
}
output "ami_id" {
  value = data.aws_ami.web.id
}
