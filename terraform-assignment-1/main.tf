resource "aws_vpc" "my-vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = var.vpc_tags
  }
}

resource "aws_subnet" "public" {
  count      = length(var.pub_sub_cidr)
  vpc_id     = aws_vpc.my-vpc.id
  cidr_block = var.pub_sub_cidr[count.index]

  tags = {
    Name = "Pub-Subnet-${count.index + 1}"
  }
}

resource "aws_subnet" "private" {
  count      = length(var.priv_sub_cidr)
  vpc_id     = aws_vpc.my-vpc.id
  cidr_block = var.priv_sub_cidr[count.index]

  tags = {
    Name = "Priv-Subnet-${count.index + 1}"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.my-vpc.id

  tags = {
    Name = var.igw_tag
  }
}

resource "aws_eip" "elastic_ip" {
  vpc = true
}
resource "aws_nat_gateway" "ninja_nat" {
  allocation_id = aws_eip.elastic_ip.allocation_id
  subnet_id     = aws_subnet.public[0].id

  tags = {
    Name = var.nat_tag
  }
}

resource "aws_route_table" "Public-RTB" {
  vpc_id = aws_vpc.my-vpc.id
  route {
    cidr_block = var.pub_route
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = var.pub_rtb_tag
  }
}

resource "aws_route_table_association" "a" {
  count          = length(var.pub_sub_cidr)
  subnet_id      = aws_subnet.public.*.id[count.index]
  route_table_id = aws_route_table.Public-RTB.id
}

resource "aws_route_table" "Private-RTB" {
  vpc_id = aws_vpc.my-vpc.id
  route {
    cidr_block     = var.priv_route
    nat_gateway_id = aws_nat_gateway.ninja_nat.id
  }

  tags = {
    Name = var.priv_rtb_tag
  }
}
resource "aws_route_table_association" "b" {
  count          = length(var.priv_sub_cidr)
  subnet_id      = aws_subnet.private.*.id[count.index]
  route_table_id = aws_route_table.Private-RTB.id
}

locals {
  inbound_ports = [80, 22]
}

resource "aws_security_group" "my-sg-01" {
  name        = "my-sg-01"
  description = "defining rules"
  vpc_id      = aws_vpc.my-vpc.id

  dynamic "ingress" {
    for_each = local.inbound_ports
    content {
      description = "description ${ingress.key}"
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


#   dynamic "egress" {
#     for_each = local.outbound_ports
#     content {
#       description = "description ${egress.key}"
#       from_port   = egress.value
#       to_port     = egress.value
#       protocol    = "tcp"
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#   }
# }

data "aws_ami" "web" {
  filter {
    name   = "state"
    values = ["available"]
  }

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20221206"]
  }

  most_recent = true

  owners = ["amazon"]

}

resource "aws_instance" "Pub-Instance" {
  ami                         = data.aws_ami.web.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.public[0].id
  key_name                    = var.key
  security_groups             = [aws_security_group.my-sg-01.id]
  associate_public_ip_address = true
  #user_data                   = file("shell.sh")
  disable_api_termination     = true
  disable_api_stop            = true


  tags = {
    Name = var.server1
  }
}

resource "aws_instance" "Priv-Instance" {
  ami                         = data.aws_ami.web.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.private[0].id
  key_name                    = var.key
  security_groups             = [aws_security_group.my-sg-01.id]
  associate_public_ip_address = false
  disable_api_termination     = true
  disable_api_stop            = true
  user_data                   = <<EOF
#!/bin/bash
echo "Copying the public key of server"
echo -e "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDoRP0nMvLEA0rXmfR/05IElgir3sBBdEehqlFDH6QNgYM2Ed1W4/rS1d8fSTtjPY6tjlzFwXSldjXnd5KDV5JEQ9KOlP1Ij7dUHXYb3Y712TT4i3MJjOENxFTd7yX9Ke2fSzXi/HszZsMXplLJt43YFszn3TG3SRyVvdnWVPKjqqZUY5/oKezHxxAjdOHfJvkwM858NsjpGxc4hLxrFYkvJ1rUZh2gEe2WUUe3eBFX14xYGv7wy4I561pYKps/bXFQZDZjp0AQERiORGdibd47jFXqx53B+glSwFaNor2olmSln0OEfaC49Ll139jobyAkjhZ0y8HL0fRluYcfhdbJlmMyfAnGXv6UI7fQUP9odF6QW0V2yDM0s3IMKVu7OsNhwa3zw8lXNnPN459auP12UBd+sMhkMtvRR5DOIVITrWzhmmWFDZObkxOndvPSvR1UPEStimyV3qTnDnBP7nNYAI9jQjgoXsxfw2nUPLyuJ+e5NuyJh+VAlW0irNcf/sU= ubuntu@ip-10-0-1-105" >> /home/ubuntu/.ssh/authorized_keys

EOF

  tags = {
    Name = var.server2
  }
}


