variable "vpc_cidr" {
  description = "give the cidr value for vpc"
  type        = string
}

variable "vpc_tags" {
  description = "give the tag of vpc"
  type        = string
}

variable "pub_sub_cidr" {
  description = "give the cidr of public subnets"
  type        = list(string)
}


variable "priv_sub_cidr" {
  description = "give the cidr of private subnets"
  type        = list(string)
}


variable "igw_tag" {}
variable "nat_tag" {}
variable "pub_route" {}
variable "pub_rtb_tag" {}
variable "priv_route" {}
variable "priv_rtb_tag" {}
#variable "ami_id" {}
variable "instance_type" {}
variable "server1" {}
variable "key" {}
variable "server2" {}



