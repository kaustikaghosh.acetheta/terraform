data "aws_ami" "web" {
  filter {
    name   = "state"
    values = ["available"]
  }

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20221206"]
  }

  most_recent = true

  owners = ["amazon"]

}

resource "aws_instance" "Pub-Instance" {
  ami                         = data.aws_ami.web.id
  instance_type               = var.instance_type
  subnet_id                   = var.pub_sub_cidr
  key_name                    = var.key
  security_groups             = [var.security_groups]
  associate_public_ip_address = var.map_pub_ip_address
  disable_api_termination     = var.disable_api_termination


  tags = {
    Name = var.instance_tag
  }
}