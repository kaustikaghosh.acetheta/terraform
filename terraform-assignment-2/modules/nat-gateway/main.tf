resource "aws_eip" "elastic_ip" {
  vpc = true
}
resource "aws_nat_gateway" "ninja_nat" {
  allocation_id = aws_eip.elastic_ip.allocation_id
  subnet_id     = var.pub_sub
  
  tags = {
    Name = var.nat_tag
  }
}
