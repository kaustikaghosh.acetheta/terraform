variable "my_vpc_id" {
    type = string
}
variable "route_cidr" {
    type = string
}
variable "igw_id" {
    type = string
}
variable "pub_rtb_tag" {
    type = string
}
variable "pub_sub_cidr" {
    type = list(string)
}
variable "nat_id" {
    type = string
}
variable "priv_rtb_tag" {
    type = string
}
variable "priv_sub_cidr" {
    type = list(string)
}
# variable "route_table_id" {
#     type = string
# }

