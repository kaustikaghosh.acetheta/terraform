variable "my_vpc_id" {
  description = "give the vpc_id"
  type        = string
  #default     = "hellomyvpc"
}


variable "pub_sub_cidr" {
  description = "give the cidr value of subnet"
  type        = list(string)
  #default     = "10.0.1.0/16"

}
variable "map_pub_ip" {
  description = "give the tag of subnet"
  type        = string
  #default     = "hello subnet"

}

variable "sub_tags" {
  description = "give the tag of subnet"
  type        = list(string)
  #default     = "hello subnet"

}

variable "pub_zone" {
  description = "give the availability-zone"
  type        = list(string)

}

variable "priv_sub_cidr" {
  description = "give the cidr of private subnets"
  type        = list(string)
}
variable "priv_zone" {
  description = "give the availability-zone"
  type        = list(string)

}
variable "priv_sub_tags" {
  description = "give the tag of subnet"
  type        = list(string)
}
