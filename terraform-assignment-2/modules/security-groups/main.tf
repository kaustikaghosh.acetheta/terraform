locals {
  inbound_ports = [80, 22 , 443]
}

resource "aws_security_group" "my-sg-01" {
  name        = "my-sg-01"
  description = "defining rules"
  vpc_id      = var.my_vpc_id

  dynamic "ingress" {
    for_each = local.inbound_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

tags = {
    Name = var.sg_tag
  }
}
